#include <SFML/Graphics.hpp>
#include <iostream>
#include <SFML/Audio.hpp>


/**********************/
/*TAMAÑO DE LA VENTANA*/
/**********************/

const int MAX_HEIGHT_SCREEM = 1080;
const int MAX_WIDTH_SCREEM  = 1920;

int main()
{
     sf::RenderWindow window(sf::VideoMode(MAX_WIDTH_SCREEM, MAX_HEIGHT_SCREEM), "Dark Knight");
     sf::Texture texture;
     if (!texture.loadFromFile("001_sprite.png"))
         return EXIT_FAILURE;
     sf::Sprite sprite_plancha(texture);

     sf::Texture texture_bg;
     if (!texture_bg.loadFromFile("01_backgroud_sprite.png"))
         return EXIT_FAILURE;
     sf::Sprite sprite_bg(texture_bg);

     while (window.isOpen())
     {
         sf::Event event;
         while (window.pollEvent(event))
         {
             if (event.type == sf::Event::Closed)
                 window.close();
         }

         window.clear();
        window.draw(sprite_bg);
        window.draw(sprite_plancha);
        window.display();
    }

    return 0;
}
