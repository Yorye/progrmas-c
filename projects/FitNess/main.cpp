#include <stdio.h>
#include <stdlib.h>
#include <strings.h>


float formula_H(int peso , int altura , int edad , float actividad);
float formula_M(int peso , int altura , int edad , float actividad);
int 	cogerDato(int max);


int main (){
	 int 	 peso, altura, edad, sexo, actividad;
	 float res;

	 system("clear");
	 system("toilet -Fborder -fpagga --metal 'FitNess'");
	 printf("\n\n");

	 printf("Bienvenido a FitNess calculator.\n");
	 printf("\n");
	 printf("¿Eres hombre o mujer?\n");
	 printf("1.Hombre\n");
	 printf("2.Mujer\n");
	 printf("\n\n");
	 sexo = cogerDato(2);
	 

	 system("clear");
	 system("toilet -Fborder -fpagga --metal 'FitNess'");
	 printf("\n\n");
	
	 printf("Neceito su peso\n");
	 scanf("%i", &peso);
	 printf("\n");

	 printf("Introduzca su altura en cm\n");
	 scanf("%i", &altura);
	 printf("\n");

	 printf("introduzca su edad\n");
	 scanf("%i", &edad);
	 
	 system("clear");
	 system("toilet -Fborder -fpagga --metal 'FitNess'");
	 printf("\n\n");
	 printf("Con que nivel de intensidad realiza ejercicio\n");
	 printf("\n");

	 printf("1.Sedentario 	 				 \n");
	 printf("2.Poco ejercicio  (1 a 3 veces por semana)\n");
	 printf("3.Ejercicio moderado  (3 a 5 veces por semana)\n");
	 printf("4.Ejrcicio intenso  (6 a 7 veces por semana)\n");
	 printf("5.Pro  (4 horas o más diarias)\n");
	 actividad = cogerDato(5);

	 if (sexo == 1){
		 if (actividad == 1){
			 res = formula_H(peso, altura, edad, 1.2);
		 }else if(actividad == 2){
			 res = formula_H(peso, altura, edad, 1.375);
		 }else if(actividad == 3){
			 res = formula_H(peso, altura, edad, 1.55);
		 }else if(actividad == 4){
			 res = formula_H(peso, altura, edad, 1.725);
		 }else { 
			 res = formula_H(peso, altura, edad, 1.9);
		 }
	 }else if (sexo == 2){
	 	 if (actividad == 1){
			 res = formula_M(peso, altura, edad, 1.2);
		 }else if(actividad == 2){
			 res = formula_M(peso, altura, edad, 1.375);
		 }else if(actividad == 3){
			 res = formula_M(peso, altura, edad, 1.55);
		 }else if(actividad == 4){
			 res = formula_M(peso, altura, edad, 1.725);
		 }else { 
			 res = formula_M(peso, altura, edad, 1.9);
		 }
	 }

	 system("clear");
	 system("toilet -Fborder -fpagga --metal 'FitNess'");
	 printf("\n\n");
	 printf("Usted tiene que consumir: %lfkcal\n", res);
	 printf("\n");
}

float formula_H(int  peso , int altura , int edad , float actividad){
	 float res;
	 res = (66+(13.7	* peso))+((5 * altura)-(6.8*edad))*actividad;
	 return res;
}

float formula_M(int peso , int altura , int edad , float actividad){
	 float res;
	 res = (655+(9.6	* peso))+((1.8 * altura)-(4.7*edad))*actividad;
	 return res;
}

int cogerDato(int max){
	 int var;
	 scanf("%d", &var);
	 while(var > max){
		 printf("Es necesario que ea un numero menor que %d\n", max);
		 scanf("%d", &var);
	 }
	 return var;
}
