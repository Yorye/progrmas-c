
#ifndef __ROBOT_H__
#define __ROBOT_H__

/*************/
/*   class   */
/*************/

class Robot {
  public:
    // Constructors/Destructors  

  Robot();

  virtual ~Robot();

  // Metodos
  void saludo() const
  {
  }

private:
  static unsigned int nrobot;
  unsigned int nserie;


  /**
   * Set the value of nrobot
   * @param new_var the new value of nrobot
   */
  void setNrobot(unsigned int new_var)
  {
    Robot::nrobot = new_var;
  }

  /**
   * Get the value of nrobot
   * @return the value of nrobot
   */
  unsigned int getNrobot()
  {
    return Robot::nrobot;
  }

  // Private attribute accessor methods
  //  


  /**
   * Set the value of nserie
   * @param new_var the new value of nserie
   */
  void setNserie(unsigned int new_var)
  {
    nserie = new_var;
  }

  /**
   * Get the value of nserie
   * @return the value of nserie
   */
  unsigned int getNserie()
  {
    return nserie;
  }

  void initAttributes();

};

#endif // ROBOT_H
