//En este fichero.h se van a definir tanto las fichas, color, tablero 

#define MAX 7

enum TPieza {
	vacio, 
	peon_n,		//♟
	torre_n,	//♜
	caballo_n,	//♞
	alfil_n,	//♝
	reina_n, 	//♛
	rei_n,		//♚
	peon_b,		//♙
	torre_b,	//♖
	caballo_b,	//♘
	alfil_b,	//♗
	reina_b		//♕
	rey_b,		//♔
};

enum TCOlor {
	null,
	blanco,
	negro,
};

int tablero[MAX][MAX];

