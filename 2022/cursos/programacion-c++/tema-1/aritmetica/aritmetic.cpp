//---------------------------------------------------------------------------------------
//---------------------------OPERACIONES SIMPLES DE ARITMËTICA---------------------------
//--------------------------------------------------------------------------------------

#include <stdio.h>

int main(void){
   int dato1, dato2, resul;
	 dato1 =20;
	 dato2 =10;
	 // SUMA 
	 resul = dato1 + dato2;
	 printf("%d + %d = %d\n", dato1, dato2, resul );
	 // RESTA
	 resul = dato1 - dato2;
	 printf("%d - %d = %d\n", dato1, dato2, resul );
	 // MULTIPLICACION
	 resul = dato1 * dato2;
	 printf("%d * %d = %d\n", dato1, dato2, resul );
	 // DIVISION
	 resul = dato1 / dato2;
	 printf("%d / %d = %d\n", dato1, dato2, resul );
}
